import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

## 1. definicija računskog grafa
# podatci i parametri
X  = tf.placeholder(tf.float32, [None])
Y_ = tf.placeholder(tf.float32, [None])
a = tf.Variable(0.0, name="a")
b = tf.Variable(0.0, name="b")

# afini regresijski model
Y = a * X + b

# kvadratni gubitak
loss = tf.reduce_mean((Y-Y_)**2)

# optimizacijski postupak: gradijentni spust
trainer = tf.train.GradientDescentOptimizer(0.1)
train_op = trainer.minimize(loss)

grads_vars = trainer.compute_gradients(loss)

grads, vs = zip(*grads_vars)
grads  = tf.Print(grads, [grads], message="grads:")

N = tf.to_float(tf.shape(X))

dL_dY = (2/N)*(Y-Y_)
an_grad_a = tf.tensordot(dL_dY,X, 1)
an_grad_b = tf.reduce_sum(dL_dY)

my_train_op = trainer.apply_gradients(grads_vars)


## 2. inicijalizacija parametara
sess = tf.Session()
sess.run(tf.initialize_all_variables())

## 3. učenje
# neka igre počnu!
x = [1, 2, 2.5, 3]
y_ = [3, 5, 6.3, 7]
for i in range(100):
  val_loss, _, val_a, val_b, y, gs_e, an_grad_a_ev, an_grad_b_ev = sess.run(
          [loss, my_train_op, a, b, Y, grads, an_grad_a, an_grad_b], feed_dict={X: x, Y_: y_})

  print(i,val_loss, val_a,val_b)
  print([v.name for v in vs])
  print(gs_e)
  print("analiticly:")
  print("grad_a-", an_grad_a_ev)
  print("grad_b-", an_grad_b_ev)

plt.scatter(x, y_)
plt.plot(x, y)
plt.show()



