import numpy as np
import tensorflow as tf
from data import *

def softmax_cross_entropy(one_hot_labels, logits):
    scaled_logits = logits - tf.reshape(tf.reduce_max(logits, axis=[1]), [-1,1])
    normalized_logits = scaled_logits - tf.reshape(tf.reduce_logsumexp(scaled_logits, axis=1), [-1, 1])
    return -tf.reduce_mean(tf.reduce_sum(one_hot_labels * normalized_logits, axis=1))

class TFLogreg:
  def __init__(self, D, C, param_delta=0.5, param_lambda=0.01):
    """Arguments:
       - D: dimensions of each datapoint 
       - C: number of classes
       - param_delta: training step
    """

    # definicija podataka i parametara:
    # definirati self.X, self.Yoh_, self.W, self.b
    self.X  = tf.placeholder(tf.float32, [None, D])
    self.Yoh_ = tf.placeholder(tf.float32, [None, C])
    self.W  = tf.get_variable(initializer=tf.random_normal_initializer, dtype=tf.float32, shape=[D, C], name="W")
    self.b = tf.get_variable(initializer=tf.random_normal_initializer, dtype=tf.float32, shape=[C], name="b")

    # formulacija modela: izračunati self.probs
    #   koristiti: tf.matmul, tf.nn.softmax
    scales = tf.matmul(self.X, self.W) + self.b
    self.probs = tf.nn.softmax(scales)

    # formulacija gubitka: self.loss
    #   koristiti: tf.log, tf.reduce_sum, tf.reduce_mean
    self.loss = softmax_cross_entropy(one_hot_labels=self.Yoh_, logits=scales) + param_lambda*tf.nn.l2_loss(self.W)
    

    # formulacija operacije učenja: self.train_step
    #   koristiti: tf.train.GradientDescentOptimizer,
    #              tf.train.GradientDescentOptimizer.minimize
    self.train_step = tf.train.GradientDescentOptimizer(0.5).minimize(self.loss)

    # instanciranje izvedbenog konteksta: self.session
    #   koristiti: tf.Session
    self.session = tf.Session()

  def train(self, X, Yoh_, param_niter):
    """Arguments:
       - X: actual datapoints [NxD]
       - Yoh_: one-hot encoded labels [NxC]
       - param_niter: number of iterations
    """
    # incijalizacija parametara
    #   koristiti: tf.initialize_all_variables
    self.session.run(tf.initialize_all_variables())

    # optimizacijska petlja
    #   koristiti: tf.Session.run
    for i in range(param_niter):
        _, loss = self.session.run([
                self.train_step,
                self.loss
                ],
                feed_dict={
                    self.X:X, 
                    self.Yoh_:Yoh_
                }
        )
        if i % 100:
            print("iter:", i, " loss:", loss)

  def eval(self, X):
    """Arguments:
       - X: actual datapoints [NxD]
       Returns: predicted class probabilites [NxC]
    """
    return self.session.run(self.probs, feed_dict={self.X:X})

if __name__ == "__main__":
  # inicijaliziraj generatore slučajnih brojeva
  np.random.seed(100)
  tf.set_random_seed(100)

  # instanciraj podatke X i labele Yoh_
  X,Y_ = sample_gmm(3, 3, 100)
  Yoh_ = class_to_onehot(Y_)

  # izgradi graf:
  tflr = TFLogreg(X.shape[1], Yoh_.shape[1], 0.1, 0.01)

  # nauči parametre:
  tflr.train(X, Yoh_, 1000)

  # dohvati vjerojatnosti na skupu za učenje
  probs = tflr.eval(X)
  Y = np.argmax(probs, axis=1)

  macro_accuracy, re_pr, CM = eval_perf_multi(Y,Y_)
  micro_re, micro_pr = list(map(np.mean, zip(*re_pr)))

  # ispiši performansu (preciznost i odziv po razredima)
  print("\nResults:")
  print("macro_acc:", macro_accuracy)
  print("micro_re:", micro_re)
  print("micro_pr:", micro_pr)
  
  print("\nPer class:")
  for i, (re, pr) in enumerate(re_pr):
      print("C:", i, end="")
      print(" re:", re, end="")
      print(" pr:", pr)

  # iscrtaj rezultate, decizijsku plohu
  rect=(np.min(X, axis=0), np.max(X, axis=0))
  graph_surface(lambda x: np.argmax(tflr.eval(x), axis=1), rect, offset=0)
  graph_data(X, Y_, Y, special=[])
  plt.show()
