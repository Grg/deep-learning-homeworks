import numpy as np
from data import *


def fcann2_train(X, Y_, param_niter=1000, param_delta=0.1, param_lambda=0.01, H=5):

    Y_oh = class_to_onehot(Y_)

    N=X.shape[0]
    D=X.shape[1]
    C=max(Y_)+1

    # initialize weights
    W1 = np.random.normal(size=(D,H))
    b1 = np.random.normal(size=H)

    W2 = np.random.normal(size=(H,C))
    b2 = np.random.normal(size=C)

    # W1 = np.ones((D,H))
    # b1 = np.ones((H))

    # W2 = np.ones((H,C))
    # b2 = np.ones((C))

    for i in range(param_niter):

        s1 = np.matmul(X, W1)+b1 
        h1 = np.maximum(s1, np.zeros_like(s1))
        assert h1.shape == (N, H)

        s2 = np.matmul(h1, W2)+b2
        assert s2.shape == (N, C)

        scores = s2

        expscores = np.exp(scores - np.max(scores)) # N x C
        
        # nazivnik sofmaksa
        sumexp = np.sum(expscores, axis=1).reshape(N,1)    # N x 1

        # logaritmirane vjerojatnosti razreda 
        probs = expscores/sumexp     # N x C

        logprobs = (scores-np.max(scores)) - np.log(sumexp)  # N x C
        assert logprobs.shape == probs.shape == (N,C)

        # gubitak
        loss  = -np.mean(np.sum(logprobs*Y_oh, axis=1))+param_lambda*(np.linalg.norm(W1)+np.linalg.norm(W2))    # scalar
         
        # dijagnostički ispis
        if i % 100 == 0:
          print("iteration {}: loss {}".format(i, loss))

        # derivacije komponenata gubitka po mjerama
        dL_ds2 = (probs-Y_oh)/N     # N x C
        assert dL_ds2.shape == (N,C)

        # gradijenti parametara
        grad_W2 = np.matmul(h1.T, dL_ds2)   # C x D (ili D x C)
        grad_b2 = np.sum(dL_ds2, axis=0)   # C x 1 (ili 1 x C)

        grad_h1 = np.matmul(dL_ds2, W2.T)
        grad_s1 = grad_h1 * np.sign(h1) # 1 ako je s1 > 0, 0 ako je s1 == 0
        
        grad_W1 = np.matmul(X.T, grad_s1)
        grad_b1 = np.sum(grad_s1, axis=0) 
        
        # poboljšani parametri
        W2 += -param_delta * grad_W2
        b2 += -param_delta * grad_b2
        W1 += -param_delta * grad_W1
        b1 += -param_delta * grad_b1

    return W1, b1, W2, b2

def fcann2_classify(X, W1, b1, W2, b2):
    N=X.shape[0]

    s1 = np.matmul(X, W1)+b1 
    h1 = np.maximum(s1, np.zeros_like(s1))

    s2 = np.matmul(h1, W2)+b2
    scores = s2

    expscores = np.exp(scores - np.max(scores)) # N x C
    
    # nazivnik sofmaksa
    sumexp = np.sum(expscores, axis=1).reshape(N,1)    # N x 1

    # logaritmirane vjerojatnosti razreda 
    probs = expscores/sumexp     # N x C

    return np.argmax(probs, axis=1)

if __name__=="__main__":
  np.random.seed(100)

  
  # get data
  X,Y_ = sample_gmm(3, 3, 100)

  # get the class predictions
  ws = fcann2_train(X, Y_)
  # Y = myDummyDecision(X)>0.5  
  Y = fcann2_classify(X, *ws)

  # graph the decision surface
  rect=(np.min(X, axis=0), np.max(X, axis=0))

  # graph_surface(myDummyDecision, rect, offset=0)
  # graph_surface(lambda x: binlogreg(x,w,b), rect, offset=0)
  graph_surface(lambda x: fcann2_classify(x,*ws), rect, offset=0)
  
  # graph the data points
  graph_data(X, Y_, Y, special=[])
  print(eval_perf_multi(Y,Y_))

  plt.show()

