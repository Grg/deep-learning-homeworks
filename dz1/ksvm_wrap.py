from sklearn import svm
from data import *
import numpy as np

class KSVMWrap:

    def __init__(self, X, Y_, param_svm_c=2, kernel='rbf', param_svm_gamma='auto'):
        '''
        Konstruira omotač i uči RBF SVM klasifikator
        X,Y_:            podatci i točni indeksi razreda
        param_svm_c:     relativni značaj podatkovne cijene
        param_svm_gamma: širina RBF jezgre
        '''
        self.clf = svm.SVC(gamma=param_svm_gamma, kernel=kernel)
        self.clf.fit(X, Y_)
        self.support = self.clf.support_


    def predict(self, X):
        '''
        Predviđa i vraća indekse razreda podataka X
        '''
        return self.clf.predict(X)


    def get_scores(self, X):
        '''
        Vraća klasifikacijske mjere podataka X
        '''
        return self.clf.predict_proba(X)

if __name__ == "__main__":
  # inicijaliziraj generatore slučajnih brojeva
  # np.random.seed(100)

  # instanciraj podatke X i labele Yoh_
  # RBF KERNEL
  X,Y_ = sample_gmm(3, 3, 100)
  Yoh_ = class_to_onehot(Y_)

  svm_clsf = KSVMWrap(X, Y_)
  Y = svm_clsf.predict(X)

  macro_accuracy, re_pr, CM = eval_perf_multi(Y,Y_)
  micro_re, micro_pr = list(map(np.mean, zip(*re_pr)))

  # ispiši performansu (preciznost i odziv po razredima)
  print("RBF:")
  print("\nResults:")
  print("macro_acc:", macro_accuracy)
  print("micro_re:", micro_re)
  print("micro_pr:", micro_pr)
  
  print("\nPer class:")
  for i, (re, pr) in enumerate(re_pr):
      print("C:", i, end="")
      print(" re:", re, end="")
      print(" pr:", pr)

  # iscrtaj rezultate, decizijsku plohu
  rect=(np.min(X, axis=0), np.max(X, axis=0))
  graph_surface(lambda x: svm_clsf.predict(x), rect, offset=0)
  graph_data(X, Y_, Y, special=svm_clsf.support)
  plt.show()
    

  # LINEAR KERNEL
  svm_clsf = KSVMWrap(X, Y_, kernel='linear')
  Y = svm_clsf.predict(X)

  macro_accuracy, re_pr, CM = eval_perf_multi(Y,Y_)
  micro_re, micro_pr = list(map(np.mean, zip(*re_pr)))

  # ispiši performansu (preciznost i odziv po razredima)
  print("LINEAR:")
  print("\nResults:")
  print("macro_acc:", macro_accuracy)
  print("micro_re:", micro_re)
  print("micro_pr:", micro_pr)
  
  print("\nPer class:")
  for i, (re, pr) in enumerate(re_pr):
      print("C:", i, end="")
      print(" re:", re, end="")
      print(" pr:", pr)

  # iscrtaj rezultate, decizijsku plohu
  rect=(np.min(X, axis=0), np.max(X, axis=0))
  graph_surface(lambda x: svm_clsf.predict(x), rect, offset=0)
  graph_data(X, Y_, Y, special=svm_clsf.support)
  plt.show()
