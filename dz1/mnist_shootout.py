import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt
from tf_deep import *

tf.app.flags.DEFINE_string('data_dir','/tmp/data/', 'Directory for storing data')
mnist = input_data.read_data_sets(tf.app.flags.FLAGS.data_dir, one_hot=True)

N=mnist.train.images.shape[0]
D=mnist.train.images.shape[1]
C=mnist.train.labels.shape[1]

def display_weights(W):
  for i, w in enumerate(np.split(W, 10, axis=1)):
      print("Digit:", i)
      plt.imshow(w.reshape(28,28), cmap=plt.get_cmap('gray'), vmin=0, vmax=1)
      plt.show()

def display_image(image):
    image = image.reshape(28,28)
    plt.imshow(image, cmap=plt.get_cmap('gray'), vmin=0, vmax=1)
    plt.show()


if __name__ == "__main__":
  # inicijaliziraj generatore slučajnih brojeva
  np.random.seed(100)
  tf.set_random_seed(100)

  # instanciraj podatke X i labele Yoh_
  Xtrain, Ytrainoh_ = mnist.train.images, mnist.train.labels
  Ytrain_ = np.argmax(Ytrainoh_, axis=1)

  Xtest, Ytestoh_ = mnist.test.images, mnist.test.labels
  Ytest_ = np.argmax(Ytestoh_, axis=1)

  # izgradi graf:
  # tflr = TFDeep([Xtrain.shape[1], Ytrainoh_.shape[1]], param_delta=0.01, param_lambda=0.00, experiment_name="jedan_sloj")
  # tflr = TFDeep([Xtrain.shape[1], Ytrainoh_.shape[1]], param_delta=0.01, param_lambda=0.00, experiment_name="jedan_sloj_early_stopping")
  # tflr = TFDeep([Xtrain.shape[1], Ytrainoh_.shape[1]], param_delta=0.01, param_lambda=0.00, experiment_name="jedan_sloj_mini_batch")
  # tflr = TFDeep([Xtrain.shape[1], Ytrainoh_.shape[1]], param_delta=1e-4, param_lambda=0.00, experiment_name="jedan_sloj_Adam", optimizer=tf.train.AdamOptimizer)
  # tflr = TFDeep([Xtrain.shape[1], Ytrainoh_.shape[1]], decay_lr=True, param_delta=1e-4, param_lambda=0.00, decay_steps=1, experiment_name="jedan_sloj_Adam_decay", optimizer=tf.train.AdamOptimizer)
  tflr = TFDeep([Xtrain.shape[1], 100, Ytrainoh_.shape[1]], param_delta=1e-4, param_lambda=0.00, batch_normalize=True, experiment_name="dva_sloj_Adam_bn", optimizer=tf.train.AdamOptimizer)
  # tflr = TFDeep([Xtrain.shape[1], 100, Ytrainoh_.shape[1]], param_delta=1e-4, param_lambda=0.00, experiment_name="dva_sloja_Adam", optimizer=tf.train.AdamOptimizer)

  # nauči parametre:
  tflr.train(Xtrain, Ytrainoh_, 10000)
  # tflr.early_stopping_train(Xtrain, Ytrainoh_, 10000)
  # tflr.train_mb(Xtrain, Ytrainoh_, epoch_num=10000, n=100)

  # h_ex, h_ex_lab = tflr.get_hardest_examples(Xtest, Ytestoh_)

  # for ex, lab in zip(h_ex, h_ex_lab):
      # print("label:", np.argmax(lab))
      # display_image(ex)

  print("TEST:")
  probs = tflr.eval(Xtest)
  Ytest = np.argmax(probs, axis=1)

  print("loss:", tflr.calc_loss(probs, Ytestoh_))
  macro_accuracy, re_pr, CM = eval_perf_multi(Ytest, Ytest_)
  micro_re, micro_pr = list(map(np.mean, zip(*re_pr)))
  # ispiši performansu (preciznost i odziv po razredima)
  print("\nResults:")
  print("macro_acc:", macro_accuracy)
  # print("micro_re:", micro_re)
  # print("micro_pr:", micro_pr)
  print("CM:")
  print(CM)
  
  # print("\nPer class:")
  # for i, (re, pr) in enumerate(re_pr):
      # print("C:", i, end="")
      # print(" re:", re, end="")
      # print(" pr:", pr)


  print("TRAIN:")
  probs = tflr.eval(Xtrain)
  Ytrain = np.argmax(probs, axis=1)

  print("loss:", tflr.calc_loss(probs, Ytrainoh_))
  macro_accuracy, re_pr, CM = eval_perf_multi(Ytrain, Ytrain_)
  micro_re, micro_pr = list(map(np.mean, zip(*re_pr)))
    

  # ispiši performansu (preciznost i odziv po razredima)
  print("\nResults:")
  print("macro_acc:", macro_accuracy)
  # print("micro_re:", micro_re)
  # print("micro_pr:", micro_pr)
  
  # print("\nPer class:")
  # for i, (re, pr) in enumerate(re_pr):
      # print("C:", i, end="")
      # print(" re:", re, end="")
      # print(" pr:", pr)

  print("CM:")
  print(CM)


