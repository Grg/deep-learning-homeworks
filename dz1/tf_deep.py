import numpy as np
import tensorflow as tf
from data import *
import os
from sklearn.metrics import accuracy_score


def softmax_cross_entropy(one_hot_labels, logits):
    scaled_logits = logits - tf.reshape(tf.reduce_max(logits, axis=[1]), [-1,1])
    normalized_logits = scaled_logits - tf.reshape(tf.reduce_logsumexp(scaled_logits, axis=1), [-1, 1])
    return -tf.reduce_mean(tf.reduce_sum(one_hot_labels * normalized_logits, axis=1))

def separate_softmax_cross_entropy(one_hot_labels, logits):
    scaled_logits = logits - tf.reshape(tf.reduce_max(logits, axis=[1]), [-1,1])
    normalized_logits = scaled_logits - tf.reshape(tf.reduce_logsumexp(scaled_logits, axis=1), [-1, 1])
    return -tf.reduce_sum(one_hot_labels * normalized_logits, axis=1)

class TFDeep:
  def __init__(self, dims, param_delta=0.1, decay_rate=1-1e-4, decay_steps=1, decay_lr=False, param_lambda=0.01, experiment_name="experiment", optimizer=tf.train.GradientDescentOptimizer, batch_normalize=False):
    """Arguments:
       - param_delta: training step
    """
    
    # definicija podataka i parametara:
    # definirati self.X, self.Yoh_, self.W, self.b
    D = dims[0]
    C = dims[-1]

    self.X  = tf.placeholder(tf.float32, [None, D])
    self.Yoh_ = tf.placeholder(tf.float32, [None, C])
    self.is_train = tf.placeholder(tf.bool)

    self.W  = [tf.get_variable(initializer=tf.random_normal_initializer, dtype=tf.float32, shape=[dims[i], dims[i+1]], name="W"+str(i)) for i in range(len(dims)-1)]
    self.b  = [tf.get_variable(initializer=tf.random_normal_initializer, dtype=tf.float32, shape=[dims[i+1]], name="b"+str(i)) for i in range(len(dims)-1)]

    self.h = []
    for i in range(len(dims)-2):
        if i == 0:
            if batch_normalize:
                self.h.append(tf.nn.relu(self.batch_norm(tf.matmul(self.X, self.W[i]) + self.b[i])))
            else:
                self.h.append(tf.nn.relu(tf.matmul(self.X, self.W[i]) + self.b[i]))
        else:
            self.h.append(tf.nn.relu(tf.matmul(self.h[i-1], self.W[i]) + self.b[i]))

    if len(dims) > 2:
        self.h.append(tf.matmul(self.h[-1], self.W[-1]) + self.b[-1])

    else:
        self.h.append(tf.matmul(self.X, self.W[0]) + self.b[0])

    self.scales = self.h[-1]


    # formulacija modela: izračunati self.probs
    #   koristiti: tf.matmul, tf.nn.softmax
    # scales = tf.matmul(self.X, self.W) + self.b
    self.probs = tf.nn.softmax(self.scales)

    # formulacija gubitka: self.loss
    #   koristiti: tf.log, tf.reduce_sum, tf.reduce_mean

    self.loss = softmax_cross_entropy(one_hot_labels=self.Yoh_, logits=self.scales) + param_lambda*tf.add_n([tf.nn.l2_loss(w) for w in self.W])


    L = tf.to_float(tf.shape(self.Yoh_)[0])
    diff = tf.argmax(self.Yoh_, axis=1)-tf.argmax(self.probs, axis=1)
    correct = L - tf.to_float(tf.count_nonzero(diff))
    self.accuracy = correct/L
    # self.accuracy = tf.Print(self.accuracy, [self.accuracy])

    tf.summary.scalar("Accuracy", self.accuracy) 
    tf.summary.scalar("Loss", self.loss) 
    
    
    # na nalazak tezih primjera
    self.separate_loss = separate_softmax_cross_entropy(one_hot_labels=self.Yoh_, logits=self.scales) + param_lambda*tf.add_n([tf.nn.l2_loss(w) for w in self.W])
    

    # formulacija operacije učenja: self.train_step
    self.optimizer = optimizer
    self.global_step = tf.Variable(0, trainable=False, name='global_step')

    if decay_lr:
        lr = tf.train.exponential_decay(param_delta, self.global_step, decay_steps, decay_rate)
    else:
        lr = param_delta
    with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
        self.train_step = self.optimizer(lr).minimize(self.loss, global_step=self.global_step)

    # instanciranje izvedbenog konteksta: self.session
    #   koristiti: tf.Session
    self.session = tf.Session()

    self.experiment_path = "./"+experiment_name+"/ckpt"
    self.experiment_dir = "./"+experiment_name
    self.saver = tf.train.Saver()
    self.swriter_train = tf.summary.FileWriter(self.experiment_dir+str("/train"), graph=tf.get_default_graph())
    self.swriter_validation = tf.summary.FileWriter(self.experiment_dir+str("/validation"), graph=tf.get_default_graph())
    # self.swriter_test = tf.summary.FileWriter(self.experiment_dir+str("/test"), graph=tf.get_default_graph())
    self.merged_s_op = tf.summary.merge_all()

  def batch_norm(self, input_tensor):
        
      # average except on the last dimension
      batch_mean, batch_var = tf.nn.moments(input_tensor, axes=list(range(len(input_tensor.shape)-1)))

      ema = tf.train.ExponentialMovingAverage(decay=0.999, zero_debias=True)
      ema_apply_op = ema.apply([batch_mean, batch_var])
      tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, ema_apply_op) 

      mean_avg = ema.average(batch_mean)
      var_avg = ema.average(batch_var)

      output = tf.cond(self.is_train, lambda: (input_tensor-batch_mean)/batch_var,lambda: (input_tensor-mean_avg)/var_avg)

      return output


  def load_or_initialize(self):
      # incijalizacija parametara
      #   koristiti: tf.initialize_all_variables
      if not os.path.exists(self.experiment_dir):
          os.makedirs(self.experiment_dir)

      latest_checkpint_path = tf.train.latest_checkpoint(self.experiment_dir)

      if latest_checkpint_path and input("load "+latest_checkpint_path+" ? y/n ") == "y":
          print("Loading")
          self.saver.restore(self.session, latest_checkpint_path) 
          begin_step = self.global_step.eval(session=self.session)
          
      else:
          self.session.run(tf.global_variables_initializer())
          begin_step=0

      tf.get_default_graph().finalize()
      return begin_step

  def early_stopping_train(self, X, Yoh_, param_niter):
    """Arguments:
       - X: actual datapoints [NxD]
       - Yoh_: one-hot encoded labels [NxC]
       - param_niter: number of iterations
    """
    # incijalizacija parametara
    #   koristiti: tf.initialize_all_variables
    best_W_ph = [tf.placeholder(tf.float32) for w in self.W]
    best_b_ph = [tf.placeholder(tf.float32) for b in self.b]

    assign_W_ops = [self.W[i].assign(best_W_ph[i]) for i in range(len(self.W))]
    assign_b_ops = [self.b[i].assign(best_b_ph[i]) for i in range(len(self.b))]

    begin_step = self.load_or_initialize()
    best_loss = np.inf
    N = X.shape[0]

    Xval = X[:N//5]
    X = X[N//5:]

    Yvaloh_ = Yoh_[:N//5]
    Yoh_ = Yoh_[N//5:]

    # optimizacijska petlja
    #   koristiti: tf.Session.run
    for i in range(begin_step, param_niter):
        if i % 10 != 0:
            _ = self.session.run([
                    self.train_step,
                    ],
                    feed_dict={
                        self.X:X, 
                        self.Yoh_:Yoh_,
                        self.is_train: True
                    }
            )
        else:
            self.session.run([
                    self.train_step,
                    ],
                    feed_dict={
                        self.X:X, 
                        self.Yoh_:Yoh_,
                        self.is_train: True
                    }
            )

            self.saver.save(self.session, self.experiment_path, global_step=i)
            loss, train_summary = self.session.run([self.loss, self.merged_s_op], feed_dict={self.X:X, self.Yoh_:Yoh_, self.is_train: False})

            self.swriter_train.add_summary(train_summary, i)

            print("iter:", i, " loss:", loss)
            val_loss, val_summary = self.session.run([self.loss,  self.merged_s_op], feed_dict={self.X: Xval, self.Yoh_:Yvaloh_, self.is_train: False})
            self.swriter_validation.add_summary(val_summary, i)


            if val_loss < best_loss:
                print("saving temporary weights at step: ", i, ", val_loss:", val_loss)
                best_Ws = self.session.run(self.W)
                best_bs = self.session.run(self.b)
                best_step = i
                val_loss = best_loss

    if begin_step < param_niter:
        self.session.run(assign_W_ops, feed_dict=dict(zip(best_W_ph, best_Ws)))
        self.session.run(assign_b_ops, feed_dict=dict(zip(best_b_ph, best_bs)))
        self.saver.save(self.session, self.experiment_path, global_step=param_niter)

  def train_mb(self, X, Yoh_, epoch_num=100, n=100):
    """Arguments:
       - X: actual datapoints [NxD]
       - Yoh_: one-hot encoded labels [NxC]
       - param_niter: number of iterations
    """
    # incijalizacija parametara
    #   koristiti: tf.initialize_all_variables
    begin_step = self.load_or_initialize()
    begin_step = begin_step // n
    D = X.shape[0]
    batch_size = D//n
    indices=list(range(D))

    # optimizacijska petlja
    #   koristiti: tf.Session.run
    for i in range(begin_step, epoch_num):
        ind_shuf = np.random.permutation(indices)
        X = X[ind_shuf]
        Yoh_ = Yoh_[ind_shuf]

        for j in range(n):
            Xbatch = X[j:j+batch_size]
            Yohbatch_ = Yoh_[j:j+batch_size]

            if j != 0 or i % 10 != 0:
                _ = self.session.run(
                        self.train_step,
                        feed_dict={
                            self.X:Xbatch, 
                            self.Yoh_:Yohbatch_,
                            self.is_train: True
                        }
                )
            else:
                _, loss, train_summary = self.session.run([
                        self.train_step,
                        self.loss,
                        self.merged_s_op
                        ],
                        feed_dict={
                            self.X:Xbatch, 
                            self.Yoh_:Yohbatch_,
                            self.is_train: True
                        }
                )
                self.saver.save(self.session, self.experiment_path, global_step=i)
                # loss, train_summary = self.session.run([self.loss, self.merged_s_op], feed_dict={self.X:X, self.Yoh_:Yoh_, self.is_train: False})
                self.swriter_train.add_summary(train_summary, global_step=i)
                print("epoch:", i, "/", epoch_num," batch: ", j, "loss:", loss)



  def train(self, X, Yoh_, param_niter):
    """Arguments:
       - X: actual datapoints [NxD]
       - Yoh_: one-hot encoded labels [NxC]
       - param_niter: number of iterations
    """
    # incijalizacija parametara
    #   koristiti: tf.initialize_all_variables
    begin_step = self.load_or_initialize()

    # optimizacijska petlja
    #   koristiti: tf.Session.run
    for i in range(begin_step, param_niter):
        if i % 10 != 0:
            _ = self.session.run(
                    self.train_step,
                    feed_dict={
                        self.X:X, 
                        self.Yoh_:Yoh_,
                        self.is_train: True,
                    }
            )
        else:
            step, loss, train_summary = self.session.run([
                    self.train_step,
                    self.loss,
                    self.merged_s_op,
                    ],
                    feed_dict={
                        self.X:X, 
                        self.Yoh_:Yoh_,
                        self.is_train: True,
                    }
            )
            # loss, train_summary = self.session.run([self.loss, self.merged_s_op], feed_dict={self.X:X, self.Yoh_:Yoh_, self.is_train: False})
            self.saver.save(self.session, self.experiment_path, global_step=i)
            self.swriter_train.add_summary(train_summary, i)
            print("iter:", i, " loss:", loss)


  def eval(self, X):
    """Arguments:
       - X: actual datapoints [NxD]
       Returns: predicted class probabilites [NxC]
    """
    return self.session.run(self.probs, feed_dict={self.X:X, self.is_train: False})


  def get_W(self):
      return self.session.run(self.W)

  def get_hardest_examples(self, X, Y_, n=3):
      losses = self.session.run(self.separate_loss, feed_dict={self.X: X, self.Yoh_: Y_, self.is_train: False})
      indices = np.argpartition(losses, -n)[-n:]
      return X[indices], Y_[indices]

  def calc_loss(self, Y, Yoh_):
      return self.session.run(self.loss, feed_dict={self.scales: Y, self.Yoh_: Yoh_})

  def count_param(self):
      params = tf.trainable_variables()
      print("params:", [v.name for v in params])
      print("total params:", sum([np.prod([d.value for d in v.shape]) for v in params]))

if __name__ == "__main__":
  # inicijaliziraj generatore slučajnih brojeva
  np.random.seed(100)
  tf.set_random_seed(100)

  # instanciraj podatke X i labele Yoh_
  X,Y_ = sample_gmm(6, 2, 10)
  Yoh_ = class_to_onehot(Y_)

  # izgradi graf:
  tflr = TFDeep([X.shape[1], Yoh_.shape[1]])

  # nauči parametre:
  tflr.train(X, Yoh_, 100)

  # dohvati vjerojatnosti na skupu za učenje
  probs = tflr.eval(X)
  Y = np.argmax(probs, axis=1)

  macro_accuracy, re_pr, CM = eval_perf_multi(Y,Y_)
  micro_re, micro_pr = list(map(np.mean, zip(*re_pr)))

  # ispiši performansu (preciznost i odziv po razredima)
  print("\nResults:")
  print("macro_acc:", macro_accuracy)
  print("micro_re:", micro_re)
  print("micro_pr:", micro_pr)
  
  print("\nPer class:")
  for i, (re, pr) in enumerate(re_pr):
      print("C:", i, end="")
      print(" re:", re, end="")
      print(" pr:", pr)

  tflr.count_param()

  # iscrtaj rezultate, decizijsku plohu
  rect=(np.min(X, axis=0), np.max(X, axis=0))
  graph_surface(lambda x: np.argmax(tflr.eval(x), axis=1), rect, offset=0)
  graph_data(X, Y_, Y, special=[])
  plt.show()
