import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt
from mnist_shootout import *
from ksvm_wrap import *

## SVM
# RBF KERNEL
Xtrain, Ytrainoh_ = mnist.train.images, mnist.train.labels
Ytrain_ = np.argmax(Ytrainoh_, axis=1)

Xtest, Ytestoh_ = mnist.test.images, mnist.test.labels
Ytest_ = np.argmax(Ytestoh_, axis=1)

svm_clsf = KSVMWrap(Xtrain, Ytrain_)
Y = svm_clsf.predict(Xtest)

macro_accuracy, re_pr, CM = eval_perf_multi(Y,Ytest_)
micro_re, micro_pr = list(map(np.mean, zip(*re_pr)))

# ispiši performansu (preciznost i odziv po razredima)
print("RBF:")
print("\nResults:")
print("macro_acc:", macro_accuracy)
print("micro_re:", micro_re)
print("micro_pr:", micro_pr)

print("\nPer class:")
for i, (re, pr) in enumerate(re_pr):
  print("C:", i, end="")
  print(" re:", re, end="")
  print(" pr:", pr)

# LINEAR KERNEL
svm_clsf = KSVMWrap(Xtrain, Ytrain_, kernel='linear')
Y = svm_clsf.predict(Xtrain)

macro_accuracy, re_pr, CM = eval_perf_multi(Y,Ytrain_)
micro_re, micro_pr = list(map(np.mean, zip(*re_pr)))

# ispiši performansu (preciznost i odziv po razredima)
print("LINEAR:")
print("\nResults:")
print("macro_acc:", macro_accuracy)
print("micro_re:", micro_re)
print("micro_pr:", micro_pr)

print("\nPer class:")
for i, (re, pr) in enumerate(re_pr):
  print("C:", i, end="")
  print(" re:", re, end="")
  print(" pr:", pr)
