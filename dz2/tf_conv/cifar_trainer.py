import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt
from tf_conv import *
import os
import pickle
import skimage as ski
import skimage.io

def shuffle_data(data_x, data_y):
  indices = np.arange(data_x.shape[0])
  np.random.shuffle(indices)
  shuffled_data_x = np.ascontiguousarray(data_x[indices])
  shuffled_data_y = np.ascontiguousarray(data_y[indices])
  return shuffled_data_x, shuffled_data_y

def unpickle(file):
  fo = open(file, 'rb')
  dict = pickle.load(fo, encoding='latin1')
  fo.close()
  return dict

def class_to_onehot(Y):
  Yoh=np.zeros((len(Y),max(Y)+1))
  Yoh[range(len(Y)),Y] = 1
  return Yoh


def load_cifar_dataset(DATA_DIR, img_height=32, img_width=32, num_channels=3):

    train_x = np.ndarray((0, img_height * img_width * num_channels), dtype=np.float32)
    train_y = []
    for i in range(1, 6):
      subset = unpickle(os.path.join(DATA_DIR, 'data_batch_%d' % i))
      train_x = np.vstack((train_x, subset['data']))
      train_y += subset['labels']
    train_x = train_x.reshape((-1, num_channels, img_height, img_width)).transpose(0,2,3,1)
    train_y = np.array(train_y, dtype=np.int32)

    subset = unpickle(os.path.join(DATA_DIR, 'test_batch'))
    test_x = subset['data'].reshape((-1, num_channels, img_height, img_width)).transpose(0,2,3,1).astype(np.float32)
    test_y = np.array(subset['labels'], dtype=np.int32)

    valid_size = 5000
    train_x, train_y = shuffle_data(train_x, train_y)
    valid_x = train_x[:valid_size, ...]
    valid_y = train_y[:valid_size, ...]
    train_x = train_x[valid_size:, ...]
    train_y = train_y[valid_size:, ...]
    data_mean = train_x.mean((0,1,2))
    data_std = train_x.std((0,1,2))

    train_x = (train_x - data_mean) / data_std
    valid_x = (valid_x - data_mean) / data_std
    test_x = (test_x - data_mean) / data_std

    train_y_oh = class_to_onehot(train_y)
    valid_y_oh = class_to_onehot(valid_y)
    test_y_oh = class_to_onehot(test_y)

    label_names = unpickle(os.path.join(DATA_DIR, 'batches.meta'))['label_names']

    return train_x, train_y, train_y_oh, valid_x, valid_y, valid_y_oh, test_x, test_y, test_y_oh, data_mean, data_std, label_names


def draw_image(img, mean, std):
    img *= std
    img += mean
    img = img.astype(np.uint8)
    ski.io.imshow(img)
    ski.io.show()


if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    # np.random.seed(100)
    # tf.set_random_seed(100)

    Xtrain, Ytrain_, Ytrainoh_, Xvalid, Yvalid_, Yvalidoh_, Xtest, Ytest_, Ytestoh_, data_mean, data_std, label_names = load_cifar_dataset("./CIFAR/")
    # Xtrain = np.concatenate((Xtrain,Xvalid), axis=0)
    # Ytrain_ = np.concatenate((Ytrain_,Yvalid_), axis=0)
    # Ytrainoh_ = np.concatenate((Ytrainoh_,Yvalidoh_), axis=0)

    # izgradi graf:
    tflr = TFConv(
          image_shape=[32, 32, 3], n_classes=10,
          param_delta=0.001, param_lambda=0.001,
          optimizer=tf.train.AdamOptimizer,
          model_name="cifar_model",
          # experiment_name="cifar_conv_HB",
          experiment_name="cifar_conv_HB_hinge",
          use_hinge_loss=True,
          decay_lr=True,
          decay_steps=18,
          decay_rate=0.9
        )


    # nauči parametre:
    # tflr.train_mb(Xtrain, Ytrainoh_, Xtest, Ytestoh_, epoch_num=10000, batch_size=250)
    tflr.train_mb(Xtrain, Ytrainoh_, Xvalid, Yvalidoh_, epoch_num=70, batch_size=5000)

    imgs, true_clss, top_3 = tflr.get_hard_images(Xtest, Ytestoh_, k=2)

    for img, cls, t3 in zip(imgs, true_clss, top_3):
        print("true:", label_names[cls])
        print("T3:", t3)
        print("top_3:", np.take(label_names, t3))
        draw_image(img, data_mean, data_std)

