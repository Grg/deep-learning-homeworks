import numpy as np
import tensorflow as tf
import tensorflow.contrib.layers as layers
import os
from sklearn.metrics import accuracy_score

def cifar_model(inputs, labels, num_classes, param_lambda):
  weight_decay = param_lambda
  conv1sz = 16
  conv2sz = 32
  fc3sz = 256
  fc4sz = 128

  with tf.contrib.framework.arg_scope([layers.convolution2d],
      kernel_size=5, stride=1, padding='SAME', activation_fn=tf.nn.relu,
      weights_initializer=layers.variance_scaling_initializer(),
      weights_regularizer=layers.l2_regularizer(weight_decay)):

    net = layers.convolution2d(inputs, conv1sz, scope='conv1')
    net = tf.nn.max_pool(net, [1, 3, 3, 1], [1, 2, 2, 1], padding='VALID', name='mp1')

    net = layers.convolution2d(net, conv2sz, scope='conv2')
    net = tf.nn.max_pool(net, [1, 3, 3, 1], [1, 2, 2, 1], padding='VALID', name='mp1')
    # ostatak konvolucijskih i pooling slojeva

  with tf.contrib.framework.arg_scope([layers.fully_connected],
      activation_fn=tf.nn.relu,
      weights_initializer=layers.variance_scaling_initializer(),
      weights_regularizer=layers.l2_regularizer(weight_decay)):

    # sada definiramo potpuno povezane slojeve
    # ali najprije prebacimo 4D tenzor u matricu
    net = layers.flatten(net)
    net = layers.fully_connected(net, fc3sz, scope='fc3')
    net = layers.fully_connected(net, fc4sz, scope='fc4')

  logits = layers.fully_connected(net, num_classes, activation_fn=None, scope='logits')

  return logits

def mnist_model(inputs, labels, num_classes, param_lambda):
  weight_decay = param_lambda
  conv1sz = 16
  conv2sz = 32
  fc3sz = 512
  with tf.contrib.framework.arg_scope([layers.convolution2d],
      kernel_size=5, stride=1, padding='SAME', activation_fn=tf.nn.relu,
      weights_initializer=layers.variance_scaling_initializer(),
      weights_regularizer=layers.l2_regularizer(weight_decay)):

    net = layers.convolution2d(inputs, conv1sz, scope='conv1')
    net = tf.nn.max_pool(net, [1, 2, 2, 1], [1, 2, 2, 1], padding='VALID', name='mp1')

    net = layers.convolution2d(net, conv2sz, scope='conv2')
    net = tf.nn.max_pool(net, [1, 2, 2, 1], [1, 2, 2, 1], padding='VALID', name='mp1')
    # ostatak konvolucijskih i pooling slojeva

  with tf.contrib.framework.arg_scope([layers.fully_connected],
      activation_fn=tf.nn.relu,
      weights_initializer=layers.variance_scaling_initializer(),
      weights_regularizer=layers.l2_regularizer(weight_decay)):

    # sada definiramo potpuno povezane slojeve
    # ali najprije prebacimo 4D tenzor u matricu
    net = layers.flatten(net)
    net = layers.fully_connected(net, fc3sz, scope='fc3')

  logits = layers.fully_connected(net, num_classes, activation_fn=None, scope='logits')

  return logits

def hinge_loss(labels, logits, margin=0.2):
    wrong, true = tf.dynamic_partition(logits, labels, num_partitions=2) 
    wrong = tf.reshape(wrong, [-1, 9])
    wrong = tf.transpose(wrong)

    true = tf.transpose(true)

    N = tf.cast(tf.shape(labels)[0], tf.float32)

    return tf.reduce_sum(tf.nn.relu(tf.transpose(wrong - true) + margin))/N


class TFConv:
  def __init__(self, image_shape, n_classes, param_delta=0.1, decay_rate=1-1e-4, decay_steps=1, decay_lr=False, param_lambda=0.01, experiment_name="experiment", optimizer=tf.train.GradientDescentOptimizer, model_name="mnist_model", use_hinge_loss=False):
    """Arguments:
       - param_delta: training step
    """
    
    # definicija podataka i parametara:
    # definirati self.X, self.Yoh_, self.W, self.b
    C = n_classes

    self.X  = tf.placeholder(tf.float32, [None, *image_shape], name="inputs")
    self.Yoh_ = tf.placeholder(tf.int32, [None, C], name="labels")

    model = globals()[model_name]
    self.logits = model(self.X, self.Yoh_, C, param_lambda)


    if not use_hinge_loss:
        self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.Yoh_, logits=self.logits))

    else:
        self.loss = hinge_loss(labels=self.Yoh_, logits=self.logits)


    tf.summary.scalar("Loss", self.loss) 
    self.evaluate(labels=self.Yoh_, logits=self.logits, num_classes=C)
    self.calculate_hard_indices(k=20)

    # add weights image
    conv1_weights = tf.contrib.framework.get_variables('conv1/weights:0')[0]
    images = tf.stack(tf.unstack(conv1_weights, axis=-1))
    tf.summary.image("conv1_weights", images, max_outputs=16)
   
    
    # formulacija operacije učenja: self.train_step
    self.optimizer = optimizer
    self.global_step = tf.Variable(0, trainable=False, name='global_step')

    if decay_lr:
        self.lr = tf.train.exponential_decay(param_delta, self.global_step, decay_steps, decay_rate, staircase=True)
    else:
        self.lr = tf.constant(param_delta)
    
    tf.summary.scalar("Learning_rate", self.lr)
    with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
        self.train_step = self.optimizer(self.lr).minimize(self.loss, global_step=self.global_step)

    # instanciranje izvedbenog konteksta: self.session
    # session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
    # self.session = tf.Session(config=session_conf)
    self.session = tf.Session()

    self.experiment_path = "./"+experiment_name+"/ckpt"
    self.experiment_dir = "./"+experiment_name
    self.saver = tf.train.Saver()
    self.swriter_train = tf.summary.FileWriter(self.experiment_dir+str("/train"), graph=tf.get_default_graph())
    self.swriter_validation = tf.summary.FileWriter(self.experiment_dir+str("/validation"), graph=tf.get_default_graph())
    self.merged_s_op = tf.summary.merge_all()

  def evaluate(self, logits, labels, num_classes):

      confusion_matrix = tf.confusion_matrix(
              labels=tf.argmax(labels, axis=1),
              predictions=tf.argmax(logits, axis=1),
              num_classes=num_classes)

      conf_diag = tf.diag_part(confusion_matrix)

      correct = tf.reduce_sum(conf_diag)
      N = tf.reduce_sum(confusion_matrix)
      accuracy = correct/N

      tf.summary.scalar("Accuracy", accuracy)

      per_class_recall = conf_diag / tf.reduce_sum(confusion_matrix, axis=1)

      for cls, recall in enumerate(tf.unstack(per_class_recall)):
          tf.summary.scalar("Class_{}_recall".format(cls), recall)


  def load_or_initialize(self):
      # incijalizacija parametara
      #   koristiti: tf.initialize_all_variables
      if not os.path.exists(self.experiment_dir):
          os.makedirs(self.experiment_dir)

      latest_checkpint_path = tf.train.latest_checkpoint(self.experiment_dir)

      if latest_checkpint_path and input("load "+latest_checkpint_path+" ? y/n ") == "y":
          print("Loading")
          self.saver.restore(self.session, latest_checkpint_path) 
          begin_step = self.global_step.eval(session=self.session)
          
      else:
          self.session.run(tf.global_variables_initializer())
          begin_step=0

      tf.get_default_graph().finalize()
      return begin_step

  def train_mb(self, X, Yoh_, Xval, Yvaloh_, epoch_num=100, n=100, batch_size=None):
    """Arguments:
       - X: actual datapoints [NxD]
       - Yoh_: one-hot encoded labels [NxC]
       - param_niter: number of iterations
    """
    # incijalizacija parametara
    #   koristiti: tf.initialize_all_variables
    D = X.shape[0]

    if batch_size is not None:
        # overrides n
        n = D // batch_size

    begin_step = self.load_or_initialize()
    begin_step = begin_step // n
    batch_size = D//n
    indices=list(range(D))

    # optimizacijska petlja
    for i in range(begin_step, epoch_num):
        ind_shuf = np.random.permutation(indices)
        X = X[ind_shuf]
        Yoh_ = Yoh_[ind_shuf]

        for j in range(n):
            Xbatch = X[j:j+batch_size]
            Yohbatch_ = Yoh_[j:j+batch_size]

            if j == 0: 
                # validation
                val_loss, val_summary = self.session.run([self.loss,  self.merged_s_op], feed_dict={self.X: Xval, self.Yoh_:Yvaloh_})
                self.swriter_validation.add_summary(val_summary, i)
                print("Validation:")
                print("epoch:", i, "/", epoch_num, "val_loss:", val_loss)
                print()

                _, loss, train_summary, g_step, lr = self.session.run([
                        self.train_step,
                        self.loss,
                        self.merged_s_op,
                        self.global_step,
                        self.lr
                        ],
                        feed_dict={
                            self.X:Xbatch, 
                            self.Yoh_:Yohbatch_,
                        }
                )
                self.saver.save(self.session, self.experiment_path, global_step=i)
                self.swriter_train.add_summary(train_summary, global_step=i)
                print("Train:")
                print("g_step:", g_step, "learning_rate", lr)
                print("epoch:", i, "/", epoch_num, "train_loss:", loss)

            else:
                _ = self.session.run(
                        self.train_step,
                        feed_dict={
                            self.X:Xbatch, 
                            self.Yoh_:Yohbatch_,
                        }
                )


  def get_W(self):
      return self.session.run(self.W)

  def calculate_hard_indices(self, k):
      _, self.top_indices = tf.nn.top_k(tf.nn.softmax_cross_entropy_with_logits(labels=self.Yoh_, logits=self.logits), k=k)
      _, self.top_3_cls = tf.nn.top_k(self.logits, k=3)

      # self.hard_imgs = tf.sparse_to_dense(top_indices, [k, 32, 32, 3], self.X)
      # self.hard_classes = tf.sparse_to_dense(top_indices, [k, 10], self.Yoh_)
      # self.hard_preds = tf.sparse_to_dense(top_indices, [k, 10], self.logits)


  def get_hard_images(self, X, Yoh_, k):
      hard_indices, top_3_cls = self.session.run(
              [self.top_indices, self.top_3_cls],
              feed_dict={
                  self.X: X,
                  self.Yoh_: Yoh_
                  }
              )

      hard_imgs = X[hard_indices]
      hard_true = np.argmax(Yoh_[hard_indices], axis=-1)

      return hard_imgs, hard_true, top_3_cls
      

