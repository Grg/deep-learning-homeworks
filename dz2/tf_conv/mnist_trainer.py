import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
import matplotlib.pyplot as plt
from tf_conv import *

tf.app.flags.DEFINE_string('data_dir','./MNIST/', 'Directory for storing data')
mnist = input_data.read_data_sets(tf.app.flags.FLAGS.data_dir, one_hot=True)

N=mnist.train.images.shape[0]
D=mnist.train.images.shape[1]
C=mnist.train.labels.shape[1]

if __name__ == "__main__":
  # inicijaliziraj generatore slučajnih brojeva
  np.random.seed(100)
  tf.set_random_seed(100)

  # instanciraj podatke X i labele Yoh_
  Xtrain, Ytrainoh_ = mnist.train.images, mnist.train.labels
  Xtrain = np.reshape(Xtrain, (Xtrain.shape[0], 28, 28, 1))[:100]
  Ytrain_ = np.argmax(Ytrainoh_, axis=1)[:100]

  Ytrain_unlabeled_ = np.argmax(Ytrainoh_, axis=1)[100:]

  Xtest, Ytestoh_ = mnist.test.images, mnist.test.labels
  Xtest = np.reshape(Xtest, (Xtest.shape[0], 28, 28, 1))
  Ytest_ = np.argmax(Ytestoh_, axis=1)

  # izgradi graf:
  tflr = TFConv(
          image_shape=[28, 28, 1], n_classes=10,
          param_delta=1e-4, param_lambda=0.00,
          experiment_name="mnist_conv_small_train",
          optimizer=tf.train.AdamOptimizer,
          model_name="mnist_model",
          use_hinge_loss=False
        )

  # nauči parametre:
  tflr.train_mb(Xtrain, Ytrainoh_, Xtest, Ytestoh_, epoch_num=100, batch_size=100)
