import string
import numpy as np
from IPython import embed

class Preprocessor():

    def __init__(self, batch_size, sequence_length):
        self.batch_size = batch_size
        self.sequence_length = sequence_length
        self.current = 0

    # ...
    # Code is nested in class definition, indentation is not representative.
    # "np" stands for numpy.

    def preprocess(self, input_file):
        with open(input_file, "r") as f:
            data = f.read() # python 2

        # count and sort most frequent characters
        symbols=list("!‘,.`:? 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\n")

        counter_dict = {letter: data.count(letter) for letter 
                in symbols}

        self.sorted_chars = sorted(counter_dict, key=lambda letter: counter_dict[letter])
        self.vocab_size = len(self.sorted_chars)

        # self.sorted chars contains just the characters ordered descending by frequency
        self.char2id = dict(zip(self.sorted_chars, range(len(self.sorted_chars))))
        # reverse the mapping
        self.id2char = {k:v for v,k in self.char2id.items()}
        # convert the data to ids
        self.x = np.array(list(map(self.char2id.get, data)))
        self.x = [x for x in self.x if x is not None]


    def encode(self, sequence):
        # returns the sequence encoded as integers
        return np.array([self.char2id[c] for c in sequence if self.char2id[c] is not None])

    def decode(self, encoded_sequence):
        # returns the sequence decoded as letters
        return np.array([self.id2char[i] for i in encoded_sequence])

    # ...
    # ...
    # Code is nested in class definition, indentation is not representative.

    def create_minibatches(self):
        self.X = self.x[:-1]
        self.Y_ = self.x[1:]

        self.num_batches = int(len(self.X) / (self.batch_size * self.sequence_length)) # calculate the number of batches
        self.last_batch_size = len(self.X) % (self.batch_size * self.sequence_length)

        # Is all the data going to be present in the batches? Why?
        # What happens if we select a batch size and sequence length larger than the length of the data?

        #######################################
        #       Convert data to batches       #
        #######################################

        if self.last_batch_size == 0:
            self.X_batches = np.split(np.array(self.X), indices_or_sections=self.num_batches)
            self.X_batches = [np.reshape(batch, (self.batch_size, self.sequence_length)) for batch in self.X_batches]

            self.Y_batches = np.split(np.array(self.Y_), indices_or_sections=self.num_batches)
            self.Y_batches = [np.reshape(batch, (self.batch_size, self.sequence_length)) for batch in self.Y_batches]

        else:
            self.X_batches = np.split(np.array(self.X[:-self.last_batch_size]), indices_or_sections=self.num_batches)
            self.X_batches = [np.reshape(batch, (self.batch_size, self.sequence_length)) for batch in self.X_batches]

            # self.X_batches.append(np.reshape(self.X[-self.last_batch_size:], (self.last_batch_size//self.sequence_length, self.sequence_length)))


            self.Y_batches = np.split(np.array(self.Y_[:-self.last_batch_size]), indices_or_sections=self.num_batches)
            self.Y_batches = [np.reshape(batch, (self.batch_size, self.sequence_length)) for batch in self.Y_batches]

            # self.Y_batches.append(np.reshape(self.Y_[-self.last_batch_size:], (-1, self.sequence_length)))

        self.num_batches = len(self.Y_batches)

    # ...
    # Code is nested in class definition, indentation is not representative.

    def next_minibatch(self):

        # handling batch pointer & reset
        # new_epoch is a boolean indicating if the batch pointer was reset
        # in this function call

        batch_x, batch_y = self.X_batches[self.current], self.Y_batches[self.current]

        new_epoch = (self.current + 1) >= self.num_batches
        self.current = (self.current + 1) % self.num_batches

        return new_epoch, np.expand_dims(batch_x, axis=-1), np.expand_dims(batch_y, axis=-1)

if __name__ == "__main__":
    pp = Preprocessor(batch_size=35, sequence_length=5)
    pp.preprocess("./selected_conversations.txt")
    enc = pp.encode("I like trains.")
    print(''.join(pp.decode(enc)))
    pp.create_minibatches()
