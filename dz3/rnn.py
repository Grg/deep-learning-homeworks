from dataset import Preprocessor
from scipy.misc import logsumexp
from IPython import embed
import numpy as np
from sklearn.metrics import accuracy_score

class RNN_network():
    def __init__(self, hidden_size, sequence_length, vocab_size, learning_rate):
        self.hidden_size = hidden_size
        self.sequence_length = sequence_length
        self.vocab_size = vocab_size
        self.learning_rate = learning_rate

        self.U = np.random.normal(0, 1e-2, (self.vocab_size, self.hidden_size)) # ... input projection
        self.W = np.random.normal(0, 1e-2, (self.hidden_size, self.hidden_size)) # ... hidden-to-hidden projection
        self.b = np.random.normal(0, 1e-2, (self.hidden_size,)) # ... input bias

        self.V = np.random.normal(0, 1e-2, (self.hidden_size, self.vocab_size)) # ... output projection
        self.c = np.random.normal(0, 1e-2, (self.vocab_size, )) # ... output bias

        # memory of past gradients - rolling sum of squares for Adagrad
        self.memory_U, self.memory_W, self.memory_V = np.zeros_like(self.U), np.zeros_like(self.W), np.zeros_like(self.V)
        self.memory_b, self.memory_c = np.zeros_like(self.b), np.zeros_like(self.c)


    def rnn_step_forward(self, x, h_prev, U, W, b):
        # A single time step forward of a recurrent neural network with a
        # hyperbolic tangent nonlinearity.

        # x - input data (minibatch size x input dimension)
        # h_prev - previous hidden state (minibatch size x hidden size)
        # U - input projection matrix (input dimension x hidden size)
        # W - hidden to hidden projection matrix (hidden size x hidden size)
        # b - bias of shape (hidden size x 1)
        # embed()

        # print(type(score))
        # embed()
        # try:
        score = h_prev @ W + x @ U + b
        assert score.shape == (x.shape[0], self.hidden_size)

        h_current = np.tanh(score)


        h_current, cache = h_current, {"score": score, "W": W, "U": U, "b": b, "h_prev": h_prev, "x": x}

        # return the new hidden state and a tuple of values needed for the backward step

        return h_current, cache


    def rnn_forward(self, x, h0, U, W, b):
        # Full unroll forward of the recurrent neural network with a
        # hyperbolic tangent nonlinearity

        # x - input data for the whole time-series (minibatch size x sequence_length x input dimension)
        # h0 - initial hidden state (minibatch size x hidden size)
        # U - input projection matrix (input dimension x hidden size)
        # W - hidden to hidden projection matrix (hidden size x hidden size)
        # b - bias of shape (hidden size x 1)
        temp_h = h0
        h = []
        cache = []
        current_sequence_length = x.shape[1]
        for i, x_step in enumerate(np.split(x, indices_or_sections=current_sequence_length, axis=1)):
            x_step = np.squeeze(x_step, axis=1)
            temp_h, temp_cache = self.rnn_step_forward(x_step, temp_h, U, W, b)
            h.append(temp_h)
            temp_cache["step"] = i
            cache.append(temp_cache)

        h = np.stack(h, axis=1)

        return h, cache

    def rnn_step_backward(self, grad_next, cache):
        # A single time step backward of a recurrent neural network with a
        # hyperbolic tangent nonlinearity.

        # grad_next - upstream gradient of the loss with respect to the next hidden state and current output
        # cache - cached information from the forward pass
        dh_dscore = 1-np.tanh(cache["score"])**2

        dL_dscore = grad_next*dh_dscore

        assert dL_dscore.shape == cache["score"].shape
        
        dL_dh_prev = dL_dscore @ np.transpose(cache["W"])
        
        dL_dW = np.transpose(cache["h_prev"]) @ dL_dscore
        assert dL_dW.shape == cache["W"].shape
        
        dL_dU = np.transpose(cache["x"]) @ dL_dscore
        assert dL_dU.shape == cache["U"].shape

        dL_db = np.sum(dL_dscore, axis=0)
        assert dL_db.shape == cache["b"].shape

        dh_prev, dU, dW, db = dL_dh_prev, dL_dU, dL_dW, dL_db

        return dh_prev, dU, dW, db


    def rnn_backward(self, dh, cache):
        # Full unroll forward of the recurrent neural network with a
        # hyperbolic tangent nonlinearity
        grad_next = 0
        dU, dW, db = [], [], [] 
        for dh_step, cache_step in zip(reversed(np.split(dh, self.sequence_length, axis=1)), reversed(cache)):
            dh_step = np.squeeze(dh_step, axis=1)
            dh_prev, dU_step, dW_step, db_step = self.rnn_step_backward(grad_next+dh_step, cache_step)
            grad_next = dh_prev
            assert dh_step.shape == grad_next.shape

            dU.append(dU_step)
            dW.append(dW_step)
            db.append(db_step)

        # dW = np.clip(dW, -5, 5)
        dU = np.clip(np.sum(np.array(dU), axis=0), -5, 5)
        dW = np.clip(np.sum(np.array(dW), axis=0), -5, 5)
        # dW = np.sum(dW, axis=0)
        db = np.clip(np.sum(np.array(db), axis=0), -5, 5)

        return dU, dW, db

    def output(self, h, V, c):
        # Calculate the output probabilities of the network
        logits = h @ V + c
        assert logits.shape[:-1] == h.shape[:-1]
        return logits
    
    def softmax_cross_entropy(self, one_hot_labels, logits):
        assert one_hot_labels.shape == logits.shape
        num_classes = logits.shape[-1]
        one_hot_labels = np.reshape(one_hot_labels, (-1, num_classes))
        logits = np.reshape(logits, (-1, num_classes))

        am_labels = np.argmax(one_hot_labels, axis=1)
        am_logits = np.argmax(logits, axis=1)
        print("acc: %1.3f" % accuracy_score(y_pred=am_logits, y_true=am_labels), end='')

        scaled_logits = logits - np.reshape(np.max(logits, axis=1), [-1,1])
        normalized_logits = scaled_logits - np.reshape(logsumexp(scaled_logits, axis=1), [-1, 1])
        loss_np= -np.mean(np.sum(one_hot_labels * normalized_logits, axis=-1))
        return loss_np

    def output_loss_and_grads(self, h, V, c, y):
        # Calculate the loss of the network for each of the outputs

        # h - hidden states of the network for each timestep.
        #     the dimensionality of h is (batch size x sequence length x hidden size (the initial state is irrelevant for the output)
        # V - the output projection matrix of dimension hidden size x vocabulary size
        # c - the output bias of dimension vocabulary size x 1
        # y - the true class distribution - a tensor of dimension
        #     batch_size x sequence_length x vocabulary size - you need to do this conversion prior to
        #     passing the argument. A fast way to create a one-hot vector from
        #     an id could be something like the following code:

        #   y[batch_id][timestep] = np.zeros((vocabulary_size, 1))
        #   y[batch_id][timestep][batch_y[timestep]] = 1

        #     where y might be a list or a dictionary.
        logits = self.output(h, V, c)

        assert logits.shape == y.shape
        loss = self.softmax_cross_entropy(y, logits)
        # grads_score = (logits - y)/(y.shape[0]*y.shape[1])
        grads_score = (logits - y)/y.shape[0]

        dV = np.sum(np.transpose(h, (0,2,1)) @ grads_score, axis=0)

        # dV_ = np.transpose(np.reshape(h, (-1, h.shape[-1]))) @ np.reshape(grads_score, (-1,grads_score.shape[-1]))
        # assert np.allclose(dV, dV_)
        # dV = np.clip(dV, -5, 5)
        assert dV.shape == V.shape
        
        dc = np.sum(grads_score, axis=(0,1))
        assert dc.shape == c.shape

        dh = grads_score @ np.transpose(V)
        assert dh.shape == h.shape

        # calculate the output (o) - unnormalized log probabilities of classes
        # calculate yhat - softmax of the output
        # calculate the cross-entropy loss
        # calculate the derivative of the cross-entropy softmax loss with respect to the output (o)
        # calculate the gradients with respect to the output parameters V and c
        # calculate the gradients with respect to the hidden layer h

        return loss, dh, dV, dc

    def update(self, dU, dW, db, dV, dc):
        # update memory matrices
        # perform the Adagrad update of parameters
        updates_U, self.memory_U = self.adagrad_updates(dU, self.memory_U)
        updates_W, self.memory_W = self.adagrad_updates(dW, self.memory_W)
        updates_b, self.memory_b = self.adagrad_updates(db, self.memory_b)
        updates_V, self.memory_V = self.adagrad_updates(dV, self.memory_V)
        updates_c, self.memory_c = self.adagrad_updates(dc, self.memory_c)

        self.U = self.U + updates_U
        self.W = self.W + updates_W
        self.b = self.b + updates_b
        self.V = self.V + updates_V
        # self.c = self.c + updates_c

        # self.U = self.U - self.learning_rate*dU
        # self.b = self.b - self.learning_rate*db
        # self.W = self.W - self.learning_rate*dW
        # self.V = self.V - self.learning_rate*dV
        # self.c = self.c - self.learning_rate*dc


    def adagrad_updates(self, grad, memory):
        memory = memory + grad**2
        return -(self.learning_rate/(1e-4+np.sqrt(memory)))*grad, memory


    def step(self, h0, x_oh, y_oh):

        h, cache = self.rnn_forward(x_oh, h0, self.U, self.W, self.b)

        loss, dh, dV, dc = self.output_loss_and_grads(h, self.V, self.c, y_oh)

        dU, dW, db = self.rnn_backward(dh, cache)
        h0 = np.reshape(h[-1,-1,:], (1,-1))

        self.update(dU, dW, db, dV, dc)
        
        print("   loss: %1.3f" % loss)

        return loss, h0

def run_language_model(dataset, max_epochs, hidden_size=100, sequence_length=30, learning_rate=1e-1, sample_every=100):
    
    vocab_size = len(dataset.sorted_chars)
    RNN = RNN_network(hidden_size, sequence_length, dataset.vocab_size, learning_rate)

    current_epoch = 0 
    batch = 0

    h0 = np.zeros((hidden_size, 1))
    h0 = np.zeros((dataset.batch_size, hidden_size))

    average_loss = 0


    while current_epoch < max_epochs: 
        e, x, y = dataset.next_minibatch()
        # h0 = np.zeros((hidden_size, 1))
        # h0 = np.zeros((dataset.batch_size, hidden_size))
         
        if e: 
            current_epoch += 1
            h0 = np.zeros((hidden_size, 1))
            h0 = np.zeros((dataset.batch_size, hidden_size))
            # why do we reset the hidden state here?

        # One-hot transform the x and y batches
        x_oh = one_hot(x, num_classes=dataset.vocab_size)
        y_oh = one_hot(y, num_classes=dataset.vocab_size)

        # Run the recurrent network on the current batch
        # Since we are using windows of a short length of characters,
        # the step function should return the hidden state at the end
        # of the unroll. You should then use that hidden state as the
        # input for the next minibatch. In this way, we artificially
        # preserve context between batches.

        # loss, h0 = RNN.step(h0, x_oh, y_oh)
        loss, h0 = RNN.step(h0, x_oh, y_oh)
        h0 = np.reshape(np.tile(h0, dataset.batch_size), (dataset.batch_size, hidden_size))

        if batch % sample_every == 0: 
            # run sampling (2.2)
            h_train = h0

            seed = 'HAN:\nIs that good or bad?\n'
            n_sample = 300

            sample(seed, n_sample, dataset, RNN)

        batch += 1

def sample(seed, n_sample, dataset, RNN):
    enc_seed = dataset.encode(seed)
    seed_onehot = np.eye(dataset.vocab_size)[enc_seed]
    # assert 20 == len(seed)
    seed_onehot = np.reshape(seed_onehot, (1, len(seed), dataset.vocab_size))

    h0 = np.zeros((1, hidden_size))

    # warmup
    letters = []
    print("sampling")
    for i in range(n_sample):
        h, cache = RNN.rnn_forward(seed_onehot, h0, RNN.U, RNN.W, RNN.b)

        h0 = h[:,-1,:]

        letter_oh = RNN.output(h, RNN.V, RNN.c)[-1,-1,:]

        letter = np.argmax(letter_oh, axis=-1)

        # letter_oh = np.exp(letter_oh)/ np.sum(np.exp(letter_oh))
        # letter_oh = np.reshape(letter_oh, (1, 71))
        # letter = np.random.choice(71, p=letter_oh.ravel())

        letters.append(letter)

        seed_onehot = np.reshape(letter_oh, (1,1,dataset.vocab_size))



    print("".join(dataset.decode(letters)))
    
    return sample

def one_hot(y, num_classes):
    old_shape = list(y.shape)
    y = y.flatten()
    if num_classes is None:
        n_values = np.max(y) + 1
    else: 
        n_values = num_classes
    old_shape[-1] = n_values
    return np.reshape(np.eye(n_values)[y], old_shape)

if __name__ == "__main__":
    batch_size = 20
    sequence_length = 50
    hidden_size = 100
    pp = Preprocessor(batch_size=batch_size, sequence_length=sequence_length)
    pp.preprocess("./selected_conversations.txt")
    pp.create_minibatches()
    run_language_model(pp, 1000, hidden_size, sequence_length, 1e-1, 100)

